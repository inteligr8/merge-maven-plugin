/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.merge;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;

/**
 * This class provides general merging support for other goals.
 * 
 * @author brian@inteligr8.com
 */
public abstract class AbstractMergeMojo extends AbstractFileContentMojo {
	
	@Parameter( property = "filesets", required = true )
	protected List<FileSet> filesets;
    
    protected boolean mergeInFileSet(boolean directory) throws MojoExecutionException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("merge " + (directory ? "directories" : "files"));
		
    	boolean didMerge = false;
		FileSetManager fsman = new FileSetManager(this.getLog());
		Path basepath = this.project.getBasedir().toPath();
		
		try {
	    	for (FileSet fileSet : this.filesets) {
	    		Path baseInputPath = this.resolveDirectory(basepath, fileSet.getDirectory(), false, "fileset input");
	    		final Path baseOutputPath = this.resolveDirectory(basepath, fileSet.getOutputDirectory(), true, "fileset output");
	    		
	    		String[] filePathsAndNames = directory ? fsman.getIncludedDirectories(fileSet) : fsman.getIncludedFiles(fileSet);
	    		for (String filePathAndName : filePathsAndNames) {
	    			Path file = baseInputPath.resolve(filePathAndName);
	    			
	    			if (Files.isDirectory(file)) {
		    			if (this.getLog().isDebugEnabled())
		    				this.getLog().debug("merging directory: " + filePathAndName);
		    			
		    			final Path baseDirInputPath = file;
		    			
	    				Files.walkFileTree(file, new FileVisitor<Path>() {
	    					public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
	    						return FileVisitResult.CONTINUE;
	    					}
	    					
	    					@Override
	    					public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
	    						return FileVisitResult.CONTINUE;
	    					}
	    					
	    					@Override
	    					public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
	    						return FileVisitResult.TERMINATE;
	    					}
	    					
	    					@Override
	    					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	    						Path filePathAndName = baseDirInputPath.relativize(file);
	    						Path tofile = baseOutputPath.resolve(filePathAndName);
	    		    			AbstractMergeMojo.this.mergeFile(file, tofile, Files.exists(tofile));
	    		    			if (!Files.exists(tofile.getParent()))
	    		    				Files.createDirectories(tofile.getParent());
	    		    			
	    						return FileVisitResult.CONTINUE;
	    					}
						});
	    			} else {
		    			Path tofile = baseOutputPath.resolve(filePathAndName);
		    			didMerge = this.mergeFile(file, tofile, didMerge);
	    			}
	    		}
	    	}
		} catch (IOException ie) {
			throw new MojoExecutionException("Execution failed due to an I/O related issue", ie);
		}
    	
    	return didMerge;
    }
    
    private boolean mergeFile(Path file, Path tofile, boolean alreadyMergedOnce) throws IOException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("merging file: '" + file + "' => '" + tofile + "'");
		
		if (!Files.exists(tofile.getParent()))
			Files.createDirectories(tofile.getParent());
		
		if (alreadyMergedOnce && this.spacing > 0) {
			StringBuilder strbuilder = new StringBuilder();
			for (int s = 0; s < this.spacing; s++)
				strbuilder.append(this.lineEnding);
			if (strbuilder.length() > 0)
				this.appendText(strbuilder.toString(), tofile);
		}
		
		alreadyMergedOnce = this.appendFileContent(file, tofile) || alreadyMergedOnce;
		return alreadyMergedOnce;
    }
    
}
