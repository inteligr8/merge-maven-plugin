
# Directory Merge Maven Plugin

This is a maven plugin that provides directory/file merging/appending support to Maven.

## Goals

| Goal                 | Description |
| -------------------- | ----------- |
| `merge-files`        | Copy all the files in each fileset into a target directory; appending contents of any duplicate filenames. |
| `merge-dirs`         | Copy all the directories in each fileset into a target directory; appending contents of duplicate filenames. |
| `append-files`       | Append all the files in each fileset into a target file. |

### Goal: `merge-files`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `filesets`             | `FileSet[]` | Yes      |         | Executes against these files. |
| `chunkSize`            | `int`       |          | `1024`  | The streaming buffer/chunk size in bytes, while merging files.  Adjust for classic speed/memory trade-off. |
| `encoding`             | `string`    |          | `utf-8` | |
| `spacing`              | `int`       |          | `0`     | How much spacing between appended files. |
| `lineEnding`           | `string`    |          | `\n`    | Typically LF (`\n`) or CRLF (`\r\n`). |

### Goal: `merge-dirs`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `filesets`             | `FileSet[]` | Yes      |         | Executes against these files. |
| `chunkSize`            | `int`       |          | `1024`  | The streaming buffer/chunk size in bytes, while merging files.  Adjust for classic speed/memory trade-off. |
| `encoding`             | `string`    |          | `utf-8` | |
| `spacing`              | `int`       |          | `0`     | How much spacing between appended files. |
| `lineEnding`           | `string`    |          | `\n`    | Typically LF (`\n`) or CRLF (`\r\n`). |

### Goal: `append-files`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `filesets`             | `FileSet[]` | Yes      |         | Executes against these files. |
| `includeHeader`        | `boolean`   |          | `false` | Should a header be added for each appended file? |
| `outputFileName`       | `string`    |          | `merged.txt` | The name of the file to output from the merged filesets; will be created in fileset output directory. |
| `chunkSize`            | `int`       |          | `1024`  | The streaming buffer/chunk size in bytes, while merging files.  Adjust for classic speed/memory trade-off. |
| `encoding`             | `string`    |          | `utf-8` | |
| `spacing`              | `int`       |          | `0`     | How much spacing between appended files. |
| `lineEnding`           | `string`    |          | `\n`    | Typically LF (`\n`) or CRLF (`\r\n`). |
