/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.merge;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.codehaus.plexus.component.annotations.Component;

/**
 * This goal merges all the files in the configured fileset, appending any
 * duplicate files.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "merge-files", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class MergeFilesMojo extends AbstractMergeMojo {

    @Override
    public void execute() throws MojoExecutionException {
    	this.getLog().debug("Executing file merge");
    	
    	this.mergeInFileSet(false);
    }
    
}
