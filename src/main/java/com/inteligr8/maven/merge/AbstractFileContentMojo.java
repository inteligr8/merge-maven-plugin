/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.merge;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * This class provides general file content support for other goals.
 * 
 * @author brian@inteligr8.com
 */
public abstract class AbstractFileContentMojo extends AbstractMojo {
	
	@Parameter( defaultValue = "${project}", readonly = true )
	protected MavenProject project;
	
	@Parameter( defaultValue = "${session}", readonly = true )
	protected MavenSession session;

	@Parameter( property = "chunkSize", required = true, defaultValue = "1024" )
	protected int chunkSize = 1024;

	@Parameter( property = "encoding", defaultValue = "utf-8" )
	protected String encoding;

	@Parameter( property = "spacing", defaultValue = "0" )
	protected int spacing;

	@Parameter( property = "lineEnding", defaultValue = "\n" )
	protected String lineEnding;
	
	protected boolean appendText(String text, Path tofile) throws IOException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("append text: '" + text + "' => " + tofile);
		
		if (text == null || text.length() == 0)
			return false;
		
		CharsetEncoder cencoder = Charset.forName(this.encoding).newEncoder();

		FileChannel targetChannel = FileChannel.open(tofile, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		try {
			Channels.newWriter(targetChannel, cencoder, this.chunkSize).append(text).close();
		} finally {
			targetChannel.close();
		}
		
		return true;
	}
	
	protected boolean appendFileContent(Path file, Path tofile) throws IOException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("append file: " + file + " => " + tofile);
		
		ByteBuffer bbuffer = ByteBuffer.allocate(this.chunkSize);

		FileChannel targetChannel = FileChannel.open(tofile, StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		try {
			FileChannel sourceChannel = FileChannel.open(file, StandardOpenOption.READ);
			try {
				while (sourceChannel.read(bbuffer) >= 0) {
					bbuffer.flip();
					targetChannel.write(bbuffer);
					bbuffer.compact();
				}
				
				bbuffer.flip();
				targetChannel.write(bbuffer);
				bbuffer.compact();
			} finally {
				sourceChannel.close();
			}
		} finally {
			targetChannel.close();
		}
		
		return true;
	}
    
    protected Path resolveDirectory(Path basepath, String directory, boolean createIfMissing, String errorName) throws IOException, MojoExecutionException {
    	if (directory == null)
    		return this.project.getBasedir().toPath();
    	
		Path path = new File(directory).toPath();
		if (!path.isAbsolute())
			path = basepath.resolve(path);
		
		if (!Files.exists(path)) {
			if (createIfMissing) {
    			Files.createDirectories(path);
			} else {
				throw new MojoExecutionException("A " + errorName + " directory does not exist: " + directory);
			}
		}
		
		if (!Files.isDirectory(path))
			throw new MojoExecutionException("A " + errorName + " does reference a directory: " + directory);
		
		return path;
    }
    
}
