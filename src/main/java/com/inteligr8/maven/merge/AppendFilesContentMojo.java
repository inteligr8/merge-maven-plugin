/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.merge;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.codehaus.plexus.component.annotations.Component;

/**
 * This goal appends all the files in the configured fileset and writes that
 * one resultant file to the configured output file.
 * 
 * @author brian@inteligr8.com
 */
@Mojo( name = "append-files", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class AppendFilesContentMojo extends AbstractFileContentMojo {
	
	@Parameter( property = "filesets", required = true )
	protected List<FileSet> filesets;

	@Parameter( property = "includeHeader", defaultValue = "false" )
	protected boolean includeHeader;
	
	@Parameter( property = "outputFileName", defaultValue = "merged.txt" )
	protected String outputFileName;

    @Override
    public void execute() throws MojoExecutionException {
    	this.getLog().debug("Executing file append");
    	
    	this.appendContentInFileSet();
    }
    
    private boolean appendContentInFileSet() throws MojoExecutionException {
    	int spacing = Math.max(this.includeHeader ? 1 : 0, this.spacing); 
    	boolean didMerge = false;
		FileSetManager fsman = new FileSetManager(this.getLog());
		Path basepath = this.project.getBasedir().toPath();
		
		try {
	    	for (FileSet fileSet : this.filesets) {
	    		Path baseInputPath = this.resolveDirectory(basepath, fileSet.getDirectory(), false, "fileset input");
	    		Path baseOutputPath = this.resolveDirectory(basepath, fileSet.getOutputDirectory(), true, "fileset output");

    			Path tofile = baseOutputPath.resolve(this.outputFileName);
    			if (!Files.exists(tofile.getParent()))
    				Files.createDirectories(tofile.getParent());
    			
	    		String[] filePathsAndNames = fsman.getIncludedFiles(fileSet);
	    		for (String filePathAndName : filePathsAndNames) {
	    			Path file = baseInputPath.resolve(filePathAndName);
	    			if (!Files.isDirectory(file)) {
	    				if (didMerge) {
	    					StringBuilder strbuilder = new StringBuilder();
	    					for (int s = 0; s < spacing; s++)
	    						strbuilder.append(this.lineEnding);
	    					if (strbuilder.length() > 0)
	    						this.appendText(strbuilder.toString(), tofile);
	    				}
	    				
	    				if (this.includeHeader) {
	    					this.appendText("# " + file.getName(file.getNameCount()-1) + this.lineEnding + this.lineEnding, tofile);
	    					didMerge = true;
	    				}
	    				
	    				didMerge = this.appendFileContent(file, tofile) || didMerge;
	    			}
	    		}
	    	}
		} catch (IOException ie) {
			throw new MojoExecutionException("Execution failed due to an I/O related issue", ie);
		}
    	
    	return didMerge;
    }
    
}
